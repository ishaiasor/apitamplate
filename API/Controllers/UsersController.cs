﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using API.ViewModels;
using BL.Services;
using COMMON;
using COMMON.DTO;
using Newtonsoft.Json.Linq;
using Unity;

namespace API.Controllers
{
    [RoutePrefix("api/users")]
    public class UsersController : BaseController
    {
        public UsersController(IUnityContainer unityContainer) : base(unityContainer)
        {
        }

        [HttpGet]
        [Route("")]
        public CustomHttpResponse<IEnumerable<UserViewModel>> Get()
        {
            logger.Trace(l => l.Message("hey there ", EventIDS.MethodEntry, EventCategory.API, args: null));
            var result = new CustomHttpResponse<IEnumerable<UserViewModel>>()
            {
                ResponseData = this.services.usersService.Get()
                .Select(user => new UserViewModel()
                {
                    Id = user.Id,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    SomeViewExtraData = "hey im extra data " + user.Id
                }),
                Success = true
            };
            logger.Trace(l => l.Message("hey there {result} ", EventIDS.MethodExit, EventCategory.BL, args: new object[] { result }));
            return result;
        }

        [HttpGet]
        [Route("{Id}")]
        public CustomHttpResponse<UserViewModel> GetById(int Id)
        {

            var user = this.services.usersService.Get(Id);
             

            return new CustomHttpResponse<UserViewModel>()
            {
                ResponseData = new UserViewModel()
                {
                    Id = user.Id,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    SomeViewExtraData = "hey im extra data " + user.Id
                },
                Success = true
            };
        }

        [HttpPost]
        [Route("")]
        public CustomHttpResponse<string> AddUser([FromBody] UserViewModel user)
        {
            services.usersService.AddUser(new UserDTO()
            {
                Id = user.Id,
                FirstName = user.FirstName,
                LastName = user.LastName,

            });
            return new CustomHttpResponse<string>()
            {
                Success = true,
                ResponseData = "user added succesfully"
            };
        }

    }
}
