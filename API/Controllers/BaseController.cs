﻿using BL.Services;
using COMMON;
using COMMON.Logs;
using COMMON.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using Unity;

namespace API.Controllers
{
    public abstract class BaseController : ApiController
    {
        protected IEnvironment environment;
        protected IServices services;
        public IActiveDirectoryService ADService;

        protected ILogger logger;

        public BaseController() { }

        public BaseController(IUnityContainer unityContainer)
        {
            this.environment = unityContainer.Resolve<IEnvironment>() ;
            this.services = unityContainer.Resolve<IServices>();
            //on domain run the real ad service
            //this.ADService = new ActiveDirectoryService(HttpContext.Current, environment);
            this.ADService = new ActiveDirectoryServiceMock(HttpContext.Current, environment);
            this.logger = TamplateLogManager.GetCurrentClassLog(ADService);
            unityContainer.RegisterInstance<ILogger>(this.logger);
        }
    }
}