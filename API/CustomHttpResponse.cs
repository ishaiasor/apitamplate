﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;

namespace API
{
    public class CustomHttpResponse<T> : IHttpActionResult
    {
        private HttpRequestMessage Request { get; set; }
      
        public bool Success { get; set; }

        public int ErrorCode  { get; set; }

        public string ErrorMessage { get; set; }

        public T ResponseData { get; set; }

        public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
        {
            HttpResponseMessage response =
                             new HttpResponseMessage(HttpStatusCode.OK);
            response.Content = new StringContent(JObject.FromObject(this).ToString());
            response.RequestMessage = Request;
            return Task.FromResult(response);
        }
    }
}
