//$(document).ready(function () {

document.addEventListener('DOMContentLoaded', (event) => {
    
    var messageLabel = document.createElement("section");
    messageLabel.id = "messages_section";
    document.body.appendChild(messageLabel);
    logMessage("hey js starting");
    getAllUsers();
});

const url = 'https://israel.consist.co.il:7540/Tamplate'//'http://localhost:3000/api/users';
actionCount = 1;


function logMessage(msg) {

    let label = document.getElementById("messages_section");
    let message = document.createElement("p");
    message.innerText = actionCount + ' : ' + msg;
    actionCount++;
    label.appendChild(message);
}






function fillTable(tableData) {

    var oldTableBody = document.querySelector('table tbody');

    var newTableBody = document.createElement("tbody");

    for (var i = 0; i < tableData.length; i++) {
        let th = document.createElement("tr");
      //  th.className = "row";
        let id_td = document.createElement("td");
        let firstName_td = document.createElement("td");
        let lastName_td = document.createElement("td");

        id_td.innerText = tableData[i].Id;
        firstName_td.innerText = tableData[i].FirstName;
        lastName_td.innerText = tableData[i].LastName;
        th.appendChild(id_td);
        th.appendChild(firstName_td);
        th.appendChild(lastName_td);

        newTableBody.appendChild(th);

    }
    oldTableBody.parentNode.replaceChild(newTableBody, oldTableBody);
}

function onErrorFromServer(err) {
    alert(err);
}

function getAllUsers() {
    httpGetAsync(url, fillTable, onErrorFromServer);
}
getAllUsers();

function postUser() {
    var id = document.getElementById("user_id").value;

    let user = {
        Id: document.getElementById("user_id").value,
        FirstName: document.getElementById("user_first_name").value,
        LastName: document.getElementById("user_last_name").value
    }

    succusCallBack = (res) => {
        logMessage("post user finished successfully");
        logMessage(res);
        getAllUsers();
    }

    errCallBack = (err) => {
        logMessage("post user finished with error");
        logMessage(err);
    }

    httpPostAsync(url, JSON.stringify(user), succusCallBack, errCallBack);
}


function httpGetAsync(theUrl, success, err) {
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function () {
        if (xmlHttp.readyState == 4 && xmlHttp.status == 200)
            filterServerResponseResult(xmlHttp.responseText, success, err);
    }
    xmlHttp.open("GET", theUrl, true); // true for asynchronous
    xmlHttp.send(null);
}

function httpPostAsync(theUrl, user, success, err) {
    var xmlHttp = new XMLHttpRequest();

    xmlHttp.onreadystatechange = function () {
        if (xmlHttp.readyState == 4 && xmlHttp.status == 200)
            filterServerResponseResult(xmlHttp.responseText, success, err);
    }
    xmlHttp.open("POST", theUrl, true); // true for asynchronous
    xmlHttp.setRequestHeader("Content-Type", "application/json")
    xmlHttp.send(user);
}

function filterServerResponseResult(responseText, success, err) {
    logMessage("processing response ...");
    let jsonResult = JSON.parse(responseText);
    if (jsonResult.Success) {
        logMessage("response is valid");
        success(jsonResult.ResponseData);

    }
    else {
        err(jsonResult)
        logMessage("response is invalid");
    }
    }
