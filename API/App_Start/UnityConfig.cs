using BL.Services;
using COMMON;
using COMMON.Environment;
using COMMON.Services;
using System.Web.Http;
using Unity;
using Unity.WebApi;

namespace API
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();

            // register all your components with the container here
            // it is NOT necessary to register your controllers

            // e.g. container.RegisterType<ITestService, TestService>();
            container.RegisterType<IEnvironment, Environment>();
            container.RegisterType<IServices, Services>();
            
            container.RegisterType<IActiveDirectoryService, ActiveDirectoryService>();
            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);
        }
    }
}