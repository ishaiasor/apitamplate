﻿using COMMON;
using COMMON.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.ExceptionHandling;
using System.Web.Http.Filters;

namespace API
{
    public class GlobalExceptionHandler : ExceptionHandler
    {
        public override void Handle(ExceptionHandlerContext context)
        {
            if(context.Exception is IGlobalException)
            {
                IGlobalException exception = (context.Exception as IGlobalException);
                context.Result = new CustomHttpResponse<string>()
                {
                    ErrorCode = (int)exception.errorCode,
                    ErrorMessage = exception.ErrorMessage,
                    Success = false
                };
                return;
            }

            context.Result = new CustomHttpResponse<Exception>()
            {
                ErrorCode = 0,
                ErrorMessage = context.Exception.Message,
                Success = false,
                ResponseData = context.Exception
            };
        }
    }
}