﻿using COMMON.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Models;

namespace COMMON.Extantions
{
    public static class UserExtentions
    {
        public static UserDTO ToUserDTO(this UserEntity user)
        {
            return new UserDTO()
            {
                Id = user.Id,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Password = user.Password,
                FullName = user.FirstName + ' ' + user.LastName
            };
        }

        public static UserEntity ToUserEntity(this UserDTO user)
        {
            return new UserEntity()
            {
                Id = user.Id,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Password = user.Password,             
            };
        }

        public static bool IsValid(this UserDTO user)
        {
            return ! string.IsNullOrEmpty(user.FirstName);
        }
    }
}
