﻿using BL.Services.Declartion;
using BL.Services.Implementation;
using COMMON;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.Services
{
    public class Services : IServices
    {
        public IUsersService usersService { get; }
        public Services(IEnvironment environment)
        {
            usersService = new UsersService(environment);
        }

       
    }
}
