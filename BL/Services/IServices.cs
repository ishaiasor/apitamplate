﻿using BL.Services.Declartion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.Services
{
    public interface IServices
    {
        IUsersService usersService { get; }
    }
}
