﻿using COMMON.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.Services.Declartion
{
    public interface IUsersService
    {
        void AddUser(UserDTO user);

        UserDTO Get(int Id);

        IEnumerable<UserDTO> Get();
    }
}
