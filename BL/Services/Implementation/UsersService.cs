﻿using BL.Services.Declartion;
using COMMON;
using COMMON.DTO;
using COMMON.Exceptions;
using COMMON.Extantions;
using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL.Services.Implementation
{
    public class UsersService : BaseService, IUsersService
    {
        public UsersService(IEnvironment environment) : base(environment)
        {
        }

        public void AddUser(UserDTO user)
        {

           
            if(!user.IsValid())
            {
                throw new ValidationException() {
                    ErrorMessage = "user is invalid",
                    errorCode = ExceptionCodes.ValidationError
                    
                };
            }

            try
            {
                using (var db = RepositoryFactory.Create(environment))
                {
                    db.usersRepository.Add(user.ToUserEntity());
                   // db.Commit();
                }
            }
            catch(Exception ex)
            {
                throw new RepositoryException() {
                    ErrorMessage = ex.Message,
                    
                };
            }                  
        }

        public UserDTO Get(int Id)
        {
            try
            {
                using (var db = RepositoryFactory.Create(environment))
                {
                    return db.usersRepository.Get(Id).ToUserDTO();
                }
            }
            catch (Exception ex)
            {
                throw new RepositoryException()
                {
                    ErrorMessage = ex.Message,
                };
            }
        }

        public IEnumerable<UserDTO> Get()
        {
            try
            {
                using (var db = RepositoryFactory.Create(environment))
                {
                   return db.usersRepository.Get().Select(user => user.ToUserDTO());          
                }
            }
            catch (Exception ex)
            {
                throw new RepositoryException()
                {
                    ErrorMessage = ex.Message,
                };
            }
        }
    }
}
