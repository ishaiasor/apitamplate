﻿using COMMON;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BL
{
    public abstract class BaseService
    {
        protected IEnvironment environment;

        public BaseService(IEnvironment environment)
        {
            this.environment = environment;
        }


    }
}
