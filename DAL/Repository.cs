﻿using COMMON;
using DAL.Repositories.Implamentation;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class Repository : IRepository , IDisposable
    {
        public IEnvironment environment { get; private set; }

    public IDbConnection connection { get; private set; }

        public IDbTransaction transaction { get; private set; }

        public IUsersRepository usersRepository { get; private set; }

        public Repository(IEnvironment environment)
        {
            this.environment = environment ;
            this.usersRepository = new UsersRepository(this);

            this.connection = new OracleConnection(environment.ConnectionString);
            /// or whatever framework you want to use ...
            /// you can use an http calls or Sql ...

            //this.connection.Open();
          //  this.transaction = connection.BeginTransaction();

        }

       

      

        public void Commit()
        {
           
            this.transaction.Commit();
        }

        public void RollBack()
        {
            this.transaction.Rollback();
        }

        public void Dispose()
        {
           if(this.transaction != null)
            {
                transaction.Rollback();
            }
            transaction = null;
            if(connection.State == ConnectionState.Open)
            {
                connection.Close();
            }

            connection = null;
        }
    }
}
