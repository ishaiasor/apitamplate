﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Models;

namespace DAL.Repositories.Implamentation
{
    public class UsersRepository : BaseRepository, IUsersRepository
    {
        static List<UserEntity> userEntities = new List<UserEntity>();

        public UsersRepository(IRepository repository) : base(repository)
        {
        }

        public void Add(UserEntity user)
        {
            userEntities.Add(user);
        }

        public UserEntity Get(int Id)
        {
            return userEntities.Find(user => user.Id == Id);
        }

        public IEnumerable<UserEntity> Get()
        {
            return userEntities;
        }

      
    }
}
