﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositories
{
    public abstract class BaseRepository
    {
        protected IDbConnection connection;
        protected IDbTransaction transaction;
        private IRepository repository;
        public BaseRepository(IRepository repository)
        {
            this.connection = repository.connection;
            this.transaction = repository.transaction;
            this.repository = repository;
        }
    }
}
