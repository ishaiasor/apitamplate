﻿using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public interface IUsersRepository
    {
        void Add(UserEntity user);

        UserEntity Get(int Id);

        IEnumerable<UserEntity> Get();
    }
}
