﻿using COMMON;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public interface IRepository :IDisposable
    {

        IEnvironment environment { get; }

        IDbConnection connection { get; }

        IDbTransaction transaction { get; }

        void Commit();

        void RollBack();

        IUsersRepository usersRepository { get; }


    }
}
