﻿using COMMON;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public static class RepositoryFactory
    {
        public static IRepository Create(IEnvironment environment) => new Repository(environment);
    }
}
