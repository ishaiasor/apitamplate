﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMMON
{
   public enum ErrorCode
    {
        InternalServerError = 500,
        SomeCustomError = 123
    }

    public enum ExceptionCodes
    {
        ValidationError = 400,
        RepositoryError = 401
    }

    public enum EventCategory
    {
        API = 1,
        BL = 2,
        DAL = 3,
        DEFAULT = 4
    }

    public enum EventIDS
    {
         Default = 1,
         MethodEntry = 2,
         MethodExit = 3

    }
}
