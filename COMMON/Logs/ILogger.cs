﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMMON.Logs
{
    public  interface ILogger
    {
        string sessionId { get; set; }

        void Log(NLog.LogLevel level, Action<ILoggerMethods> logAction);
        void Trace(Action<ILoggerMethods> logAction);

        void Debug(Action<ILoggerMethods> logAction);

        void Info(Action<ILoggerMethods> logAction);

        void Error(Action<ILoggerMethods> logAction);

        void Fatal(Action<ILoggerMethods> logAction);
    }
}
