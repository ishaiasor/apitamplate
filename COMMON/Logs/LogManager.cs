﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.CompilerServices;
using System.Diagnostics;
using COMMON.Services;
using NLog;

namespace COMMON.Logs
{
    

    public static class TamplateLogManager
    {

        private static class LogManager
        {

            [MethodImpl(MethodImplOptions.NoInlining)]
            public static ILogger GetCurrentClassLogger()
            {
                var stackFrame = new StackFrame(1, false);
                return GetLogger(stackFrame.GetMethod().DeclaringType.ToString());
            }

            public static ILogger GetLogger(string name)
            {
                return new NLogExtender(NLog.LogManager.GetLogger(name));
            }
        }

        [MethodImpl(MethodImplOptions.NoInlining)]
        public static ILogger GetCurrentClassLog(IActiveDirectoryService ADService,string sessionId = null)
        {
            var stackFrame = new StackFrame(1, false);
            return GetLogger(stackFrame.GetMethod().DeclaringType.ToString(), ADService, sessionId);
        }
        private static ILogger GetLogger(string name, IActiveDirectoryService adService, string sessionId = null)
        {
            sessionId = sessionId != null ? sessionId : Guid.NewGuid().ToString();

            var logger = LogManager.GetLogger(name);
            logger.sessionId = sessionId;
            MappedDiagnosticsLogicalContext.Set("SessionId", sessionId);
            MappedDiagnosticsLogicalContext.Set("UserName", adService.UserName);
            MappedDiagnosticsLogicalContext.Set("Url", adService.Url);
            MappedDiagnosticsLogicalContext.Set("IP", adService.GetIdentifiedIpAddress());
            MappedDiagnosticsLogicalContext.Set("UserId", adService.AccountId());

            return logger;
        }
    }
}
