﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;

namespace COMMON.Logs
{
    public class NLogExtender : ILogger
    {
        private readonly NLog.ILogger _nlog;
        private readonly ILoggerMethods _m_trace;
        private readonly ILoggerMethods _m_debug;
        private readonly ILoggerMethods _m_info;
        private readonly ILoggerMethods _m_warn;
        private readonly ILoggerMethods _m_error;
        private readonly ILoggerMethods _m_fatal;

        public string sessionId { get; set; }

        private class NLogExtenderMethods : ILoggerMethods
        {
            private NLog.ILogger _nlog;
            private LogLevel _level;

            public NLogExtenderMethods(NLog.ILogger nlog, LogLevel level)
            {
                this._nlog = nlog;
                this._level = level;
            }
            public void Message(LogEventInfo info)
            {
                info.Level = _level;
                info.LoggerName = _nlog.Name;
                _nlog.Log(typeof(NLogExtenderMethods), info);
            }

            public void Message(string message, params object[] args)
            {
                Message(LogEventInfo.Create(_level, _nlog.Name, NLog.LogManager.LogFactory.DefaultCultureInfo, message, args));
            }

            public void Message(Exception exception, string message, params object[] args)
            {
                var info = LogEventInfo.Create(_level, _nlog.Name, NLog.LogManager.LogFactory.DefaultCultureInfo, message, args);
                info.Exception = exception;
                Message(info);
            }
        }

        public NLogExtender(NLog.ILogger nlog)
        {
            this._nlog = nlog;
            this._m_trace = new NLogExtenderMethods(nlog, LogLevel.Trace);
            this._m_debug = new NLogExtenderMethods(nlog, LogLevel.Debug);
            this._m_info = new NLogExtenderMethods(nlog, LogLevel.Info);
            this._m_warn = new NLogExtenderMethods(nlog, LogLevel.Warn);
            this._m_error = new NLogExtenderMethods(nlog, LogLevel.Error);
            this._m_fatal = new NLogExtenderMethods(nlog, LogLevel.Fatal);
        }

        public void Debug(Action<ILoggerMethods> logAction)
        {
            if (_nlog.IsDebugEnabled)
                logAction(this._m_debug);
        }

        public void Error(Action<ILoggerMethods> logAction)
        {
            if (_nlog.IsErrorEnabled)
                logAction(this._m_error);
        }

        public void Fatal(Action<ILoggerMethods> logAction)
        {
            if (_nlog.IsFatalEnabled)
                logAction(this._m_fatal);
        }

        public void Info(Action<ILoggerMethods> logAction)
        {
            if (_nlog.IsInfoEnabled)
                logAction(this.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         _m_info);
        }

        public void Trace(Action<ILoggerMethods> logAction)
        {
            if (_nlog.IsTraceEnabled)
                logAction(this._m_trace);
        }

        public void Log(LogLevel level, Action<ILoggerMethods> logAction)
        {
            if (_nlog.IsEnabled(level))
            {
                if(level == LogLevel.Trace)
                  logAction(this._m_trace);
                if (level == LogLevel.Debug)
                    logAction(this._m_debug);
                if (level == LogLevel.Info)
                    logAction(this._m_info);
                if (level == LogLevel.Warn)
                    logAction(this._m_warn);
                if (level == LogLevel.Error)
                    logAction(this._m_error);
                if (level == LogLevel.Fatal)
                    logAction(this._m_fatal);                   
            }
        }      
    }
}
