﻿using COMMON.Logs;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMMON
{
    public static class LoggerMethodExtensions
    {
        public static void Message(this ILoggerMethods m,string message,EventIDS eventId = EventIDS.Default, EventCategory eventCategory = EventCategory.DEFAULT,params object[] args)
        {
            var log = LogEventInfo.Create(LogLevel.Trace, string.Empty, NLog.LogManager.LogFactory.DefaultCultureInfo, message, args);
            log.Properties.Add("__EventSource",$"{ eventCategory.ToString()}");
            log.Properties.Add("__eventId", (int)eventId);
            m.Message(log);
        }
    }
}
