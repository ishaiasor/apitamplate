﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;

namespace COMMON.Logs
{
    public interface ILoggerMethods
    {
        void Message(LogEventInfo info);
        //void Message(string message, params object[] args);

        //void Message(Exception exception, string message, params object[] args);
    }
}
