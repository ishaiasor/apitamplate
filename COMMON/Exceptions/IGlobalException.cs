﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMMON.Exceptions
{
    public interface IGlobalException
    {
        ExceptionCodes errorCode { get;}

        string ErrorMessage { get;}

        string ExceptionName { get;}
    }
}
