﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMMON.Exceptions
{
    public class RepositoryException : Exception, IGlobalException
    {
        public ExceptionCodes errorCode { get; set; }
        public string ErrorMessage { get; set; }
        public string ExceptionName { get; set; }

        public RepositoryException()
        {
            errorCode = ExceptionCodes.RepositoryError;
            ErrorMessage = "repository exception";
            ExceptionName = "RepositoryException";
        }
        public RepositoryException(ExceptionCodes exceptionCode, string ErrorMessage, string ExceptionName)
        {
            this.errorCode = errorCode;
            this.ErrorMessage = ErrorMessage;
            this.ExceptionName = ExceptionName;
        }
    }
}

