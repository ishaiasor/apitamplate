﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMMON.Exceptions
{
    public class ValidationException : Exception, IGlobalException
    {
        public ExceptionCodes errorCode { get; set; }
        public string ErrorMessage { get; set; }
        public string ExceptionName { get; set; }

        public ValidationException()
        {
            errorCode = ExceptionCodes.ValidationError;
            ErrorMessage = "validation error message";
            ExceptionName = "validation error ";
        }
        public ValidationException(ExceptionCodes exceptionCode, string ErrorMessage, string ExceptionName)
        {
            this.errorCode = errorCode;
            this.ErrorMessage = ErrorMessage;
            this.ExceptionName = ExceptionName; 
        }
    }
}
