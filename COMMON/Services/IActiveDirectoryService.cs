﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMMON.Services
{
    public interface IActiveDirectoryService
    {
        string domainName { get; set; }

        string UserName { get; }
        string Url { get; }
        string GetIdentifiedUserId();
        string GetIdentifiedDomain();
        string GetIdentifiedIpAddress();
        System.DirectoryServices.PropertyCollection Data();

        string FullName();

        byte[] Picture();

        string AccountId();
    }
}
