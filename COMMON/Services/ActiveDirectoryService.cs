﻿using System;
using System.Collections.Generic;
using System.Data;
using System.DirectoryServices;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace COMMON.Services
{
    public class ActiveDirectoryService : IActiveDirectoryService
    {
        private HttpContext context;

        public ActiveDirectoryService(HttpContext context,IEnvironment environment)
        {
            this.context = context;
            this.domainName = environment.DomainName;
        }

        public string UserName => context.User.Identity.Name;

        public string Url => context.Request.Url.ToString();

        public string domainName { get; set; } = "";

        public string AccountId()
        {
            return Data()["sAMAccountName"].Value as string;
        }

        public System.DirectoryServices.PropertyCollection Data()
        {
            string filter = string.Format("(&(ObjectClass={0})(sAMAccountName={1}))", "user", GetIdentifiedUserId());
            DirectoryEntry adRoot = new DirectoryEntry($@"LDAP://DC={domainName},dc=loc");
            DirectorySearcher searcher = new DirectorySearcher(adRoot);
            searcher.SearchScope = SearchScope.Subtree;
            searcher.ReferralChasing = ReferralChasingOption.All;
            searcher.PropertiesToLoad.AddRange(new string[] { "MemberOf","displayName","forwardAddress","primaryTelexNumber","adminDescription"});
            searcher.Filter = filter;
            DirectoryEntry directoryEntry = null;

            try
            {
                SearchResult result = searcher.FindOne();
                if (result != null)
                {
                    directoryEntry = result.GetDirectoryEntry();

                    return directoryEntry.Properties;
                }
            }
            catch(Exception ex)
            {
                return null;
            }
            return null;
        }

        public string FullName()
        {
            return Data()["displayName"].Value as string;
        }

        public string GetIdentifiedDomain()
        {
            string name = context.User.Identity.Name;
            string[] tmp = name.Split(new char[] { '\\' }, StringSplitOptions.RemoveEmptyEntries);
            if (tmp.Length > 0)
            {
                return tmp[0];
            }
            return string.Empty;
        }

        public string GetIdentifiedIpAddress()
        {
            return context.Request.Url.ToString();
        }

        public string GetIdentifiedUserId()
        {
            string name = context.User.Identity.Name;
            string[] tmp = name.Split(new char[] { '\\' }, StringSplitOptions.RemoveEmptyEntries);
            if(tmp.Length >0 )
            {
                return tmp[tmp.Length - 1];
            }
            return string.Empty;
        }

        public byte[] Picture()
        {
            return Data()["thumbnailPhoto"].Value as byte[];
        }
    }
}
