﻿using System;
using System.Collections.Generic;
using System.DirectoryServices;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace COMMON.Services
{
    public  class ActiveDirectoryServiceMock : IActiveDirectoryService
    {
        private HttpContext context;
        private IEnvironment environment;

        public ActiveDirectoryServiceMock(HttpContext context, IEnvironment environment)
        {
            this.context = context;
            this.environment = environment;
        }

        public string domainName { get; set; }

        public string UserName => "user name mock";

        public string Url => "url mock";

        public string AccountId()
        {
            return "accountid mock";
        }

        public PropertyCollection Data()
        {
            return null;
        }

        public string FullName()
        {
            return "full name mock";
        }

        public string GetIdentifiedDomain()
        {
            return "domain mock";
        }

        public string GetIdentifiedIpAddress()
        {
          return  "user ip mock";
        }

        public string GetIdentifiedUserId()
        {
            return "user id mock";
        }

        public byte[] Picture()
        {
            return null;
        }
    }
}
