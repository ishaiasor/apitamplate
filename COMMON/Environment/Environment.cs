﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMMON.Environment
{
    public class Environment : IEnvironment
    {
        //example of lazy loading from web.config / app.config
        private string _connectionString = null;
        public string ConnectionString
        {
            get
            {
                if(_connectionString == null)
                {
                    _connectionString = ConfigurationManager.ConnectionStrings["ConnectionStringDev"].ToString();
                }
                return _connectionString;
            }
            set { this._connectionString = value; }
        }

        //example of loading from web.config 
        public string SomeConfigData { get { return ConfigurationManager.AppSettings["SomeConfigData"].ToString(); } }

        public string DomainName => ConfigurationManager.AppSettings["DomainName"].ToString();
    }
}
