﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COMMON
{
    public interface IEnvironment
    {
         string ConnectionString { get; set; }

         string SomeConfigData { get; }

        string DomainName { get; }
    }
}
